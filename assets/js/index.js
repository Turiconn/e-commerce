// // Navbar JS
// const menu = document.querySelector(".menu");
// const menuMain = menu.querySelector(".menu-main");
// const goBack = menu.querySelector(".go-back");
// const menuTrigger = document.querySelector(".mobile-menu-trigger");
// const closeMenu = menu.querySelector(".mobile-menu-close");
// let subMenu;
// menuMain.addEventListener("click", (e) => {
//   if (!menu.classList.contains("active")) {
//     return;
//   }
//   if (e.target.closest(".menu-item-has-children")) {
//     const hasChildren = e.target.closest(".menu-item-has-children");
//     showSubMenu(hasChildren);
//   }
// });
// goBack.addEventListener("click", () => {
//   hideSubMenu();
// });
// menuTrigger.addEventListener("click", () => {
//   toggleMenu();
// });
// closeMenu.addEventListener("click", () => {
//   toggleMenu();
// });
// document.querySelector(".menu-overlay").addEventListener("click", () => {
//   toggleMenu();
// });
// function toggleMenu() {
//   menu.classList.toggle("active");
//   document.querySelector(".menu-overlay").classList.toggle("active");
// }
// function showSubMenu(hasChildren) {
//   subMenu = hasChildren.querySelector(".sub-menu");
//   subMenu.classList.add("active");
//   subMenu.style.animation = "slideLeft 0.5s ease forwards";
//   const menuTitle =
//     hasChildren.querySelector("i").parentNode.childNodes[0].textContent;
//   menu.querySelector(".current-menu-title").innerHTML = menuTitle;
//   menu.querySelector(".mobile-menu-head").classList.add("active");
// }

// function hideSubMenu() {
//   subMenu.style.animation = "slideRight 0.5s ease forwards";
//   setTimeout(() => {
//     subMenu.classList.remove("active");
//   }, 300);
//   menu.querySelector(".current-menu-title").innerHTML = "";
//   menu.querySelector(".mobile-menu-head").classList.remove("active");
// }

// window.onresize = function () {
//   if (this.innerWidth > 991) {
//     if (menu.classList.contains("active")) {
//       toggleMenu();
//     }
//   }
// };

// document.querySelectorAll(".tab-link").forEach((a) => {
//   a.addEventListener("mouseover", (e) => {
//     e.target.nextElementSibling.classList.add("animation", "enable");
//   });
//   a.addEventListener("mouseleave", (e) => {
//     if (document.querySelectorAll(".animation.enable"))
//       document
//         .querySelectorAll(".animation.enable")
//         .forEach((a) => a.classList.remove("animation", "enable"));
//   });
// });




// Scroll Top Function

var goTop = document.getElementById("goTop");
window.onscroll = function () { scrollFunction() };
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    goTop.style.display = "block";
  } else {
    goTop.style.display = "none";
  }
}
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


// Basket and Details JS

$(document).ready(function () {
  $(".details_color").on("click", "label", function () {
    var i = 0;
    var image_src = $(this).find("img")[0].src;
    $(".show").find("img").attr("src", image_src);

    $("#small-img-roll")
      .find(".show-small-img")
      .each(function () {
        i++;
        console.log($(this).attr("src"));
        $(this).attr("src", image_src);
      });
  });

  let i = $(".link-text span").text();
  if (i == 0) {
    $(".thereis-noproduct").css({ display: "block" });
    $("#carousel-wrapper").css({ display: "none" });
  } else {
    $(".thereis-noproduct").css({ display: "none" });
    $("#carousel-wrapper").css({ display: "flex" });
  }
});

$(".details_add").on("click", function () {
  let i = $(".shopcart-dropdown .count").text();
  // i++;
  console.log(i++);
  $(".shopcart-dropdown .count").text(i);

  if (i == 0) {
    $(".thereis-noproduc").css({ display: "block" });
    $("#carousel-wrapper").css({ display: "none" });
  } else {
    $(".thereis-noproduct").css({ display: "none" });
    $("#carousel-wrapper").css({ display: "flex" });
  }
});

$(".discount-code").on("keyup", "input", function () {
  if ($(this).val() != "") {
    $(".discount-code button").attr("disabled", false);
  } else {
    $(".discount-code button").attr("disabled", true);
  }
});

// $(".basket-quantity input").val(1);
// var quantity = $(".basket-quantity input").val();

// $(".basket-quantity").on("click", ".decrease", function () {
//   var quantity = $(this).next().val();
//   if (quantity > 1) {
//     $(this)
//       .next()
//       .val(parseInt(quantity) - 1);
//   }
// });

// $(".basket-quantity").on("click", ".increase", function () {
//   var quantity = $(this).prev().val();
//   $(this)
//     .prev()
//     .val(parseInt(quantity) + 1);
// });

// $(".basket-trash").on("click", ".fa-trash-alt", function () {
//   $(this).parents(".single_basket").remove();
// });



// Basket mini slider

const text1_options = [
  "White satin party Dress",
  "White Shirt with 70% cotton material",
  "Part black midi dress with strech size",
  "Asos design dark blue jeans",
];
const text2_options = ["Size: 14", "Size: 38", "Size: 36", "Size: 37"];
const text3_options = ["48$", "55.20$", "24$", "50$"];
const image_options = [
  "images/main1.jfif",
  "images/main2.jfif",
  "images/main3.jfif",
  "images/main4.jfif",
];
var i = 0;
const currentOptionText1 = document.getElementById("current-option-text1");
const currentOptionText2 = document.getElementById("current-option-text2");
const currentOptionText3 = document.getElementById("current-option-text3");
const currentOptionImage = document.getElementById("image");
const carousel = document.getElementById("carousel-wrapper");
const mainMenu = document.getElementById("menu");
const optionPrevious = document.getElementById("previous-option");
const optionNext = document.getElementById("next-option");

currentOptionText1.innerText = text1_options[i];
currentOptionText2.innerText = text2_options[i];
currentOptionText3.innerText = text3_options[i];
currentOptionImage.style.backgroundImage = "url(" + image_options[i] + ")";

optionNext.onclick = function () {
  i = i + 1;
  i = i % text1_options.length;
  currentOptionText1.dataset.nextText = text1_options[i];
  currentOptionText2.dataset.nextText = text2_options[i];
  currentOptionText3.dataset.nextText = text3_options[i];

  carousel.classList.add("anim-next");

  setTimeout(() => {
    currentOptionImage.style.backgroundImage = "url(" + image_options[i] + ")";
  }, 455);

  setTimeout(() => {
    currentOptionText1.innerText = text1_options[i];
    currentOptionText2.innerText = text2_options[i];
    currentOptionText3.innerText = text3_options[i];
    carousel.classList.remove("anim-next");
  }, 455);
};

optionPrevious.onclick = function () {
  if (i === 0) {
    i = text1_options.length;
  }
  i = i - 1;
  currentOptionText1.dataset.previousText = text1_options[i];
  currentOptionText2.dataset.previousText = text2_options[i];
  currentOptionText3.dataset.previousText = text3_options[i];

  carousel.classList.add("anim-previous");

  setTimeout(() => {
    currentOptionImage.style.backgroundImage = "url(" + image_options[i] + ")";
  }, 455);

  setTimeout(() => {
    currentOptionText1.innerText = text1_options[i];
    currentOptionText2.innerText = text2_options[i];
    currentOptionText3.innerText = text3_options[i];
    carousel.classList.remove("anim-previous");
  }, 455);
};


